chk_front(_,[]):-fail.
chk_front([B|[]],C):-
    chk_front(B, C).
chk_front([A|B], [H|T]):-
    chk_front(A, [H|T]),
    chk_front(B, T).
chk_front(Char, [H|_]):-
    atom_chars(H, [Char|_]).