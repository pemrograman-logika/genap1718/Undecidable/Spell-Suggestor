create_dict(L):-
    setup_call_cleanup(open('kebi.txt',read, In), readData(In,L), close(In)).

readData(In, L) :-
    read_term(In, H, []),
    (   H == end_of_file
    ->  L = []
    ;   L = [H|T],
        readData(In,T)
    ).

get_prefix(_, [], []) :- !.
get_prefix(0, _, []) :- !.
get_prefix(N, [H|T], [H|Tn]) :-
    M is N - 1,
    get_prefix(M, T, Tn), !.

correct(Word,SuggestionList) :-
    atom_chars(Word, CharList),
    findall(Prefix, prefix(Prefix, CharList), [_|BagOfPrefix]),
    findall(Suffix, suffix(Suffix, CharList), Bag), reverse(Bag, BagOfSuffix),
    create_dict(L),
    loop_by_prefix(BagOfPrefix, L, List),
    loop_by_suffix(BagOfSuffix, List, SuggestionList).

get_word_with_prefix(PrefixCharList, Dict, Output) :- get_word_with_prefix(PrefixCharList, Dict, [], Output).
get_word_with_prefix(PrefixCharList, [H], List, Output) :-
    atom_chars(H, DictCharList),
    prefix_check(PrefixCharList, DictCharList, Out),
    append(Out, List, Output),!.
get_word_with_prefix(PrefixCharList, [H|T], List, Output) :-
    atom_chars(H, DictCharList),
    prefix_check(PrefixCharList, DictCharList, Out),
    append(Out, List, X),
    get_word_with_prefix(PrefixCharList, T, X, Output).

prefix_check(PrefixCharList, DictCharList, [H]) :-
    prefix(PrefixCharList, DictCharList),!,
    atom_chars(H, DictCharList).
prefix_check(_, _, []).

loop_by_prefix([H], Dict, Out) :- 
    get_word_with_prefix(H,Dict,SuggestionList),
    ( SuggestionList \= []
    ->  append(SuggestionList, [], Out)
    ;   append(Dict, [], Out)
    ),!.
loop_by_prefix([H|T], Dict, SuggestionList) :-
    get_word_with_prefix(H,Dict,Out),
    ( Out \= []
    ->  loop_by_prefix(T,Out,SuggestionList)
    ;   append(Dict, [], SuggestionList)
    ).

suffix(Suffix, List) :-
    Front = [_|_],
    Suffix = [_|_],
    append(Front, Suffix, List).

get_word_with_suffix(SuffixCharList, Dict, Output) :- get_word_with_suffix(SuffixCharList, Dict, [], Output).
get_word_with_suffix(SuffixCharList, [H], List, Output) :-
    atom_chars(H, DictCharList),
    suffix_check(SuffixCharList, DictCharList, Out),
    append(Out, List, Output),!.
get_word_with_suffix(SuffixCharList, [H|T], List, Output) :-
    atom_chars(H, DictCharList),
    suffix_check(SuffixCharList, DictCharList, Out),
    append(Out, List, X),
    get_word_with_suffix(SuffixCharList, T, X, Output).

suffix_check(SuffixCharList, DictCharList, [H]) :-
    suffix(SuffixCharList, DictCharList),!,
    atom_chars(H, DictCharList).
suffix_check(_, _, []).

loop_by_suffix([H], Dict, Out) :- 
    get_word_with_suffix(H,Dict,SuggestionList),
    ( SuggestionList \= []
    ->  append(SuggestionList, [], Out)
    ;   append(Dict, [], Out)
    ),!.
loop_by_suffix([H|T], Dict, SuggestionList) :-
    get_word_with_suffix(H,Dict,Out),
    ( Out \= []
    ->  loop_by_prefix(T,Out,SuggestionList)
    ;   append(Dict, [], SuggestionList)
    ).