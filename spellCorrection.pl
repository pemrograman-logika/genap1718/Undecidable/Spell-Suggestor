word(sama).
word(saya).
word(suka).


char(a).
char(m).
char(y).

correct(A,B) :- atom_chars(A,C), correctHelp(C,[],B).

correctHelp([],Y,Z) :- atom_chars(X,Y), word(X), Z=X.
correctHelp([H|T],Y,Z) :- insTail(Y,H,L), allWordCode(W), subList(L,W), !, correctHelp(T,L,Z).
correctHelp([H|T],Y,Z) :- char(X), insTail(Y,X,L), allWordCode(W), subList(L,W), correctHelp(T,L,Z).

allWordCode(X) :- word(Y), atom_chars(Y,Z), X=Z.

insTail([], N, [N]).
insTail([H|T], N, [H|R]) :- insTail(T, N, R).

equal([], []).
equal([H1|R1], [H2|R2]):-
    H1 = H2,
    equal(R1, R2).

subList([],_).
subList([X|XS], [X|XSS]) :- subList(XS, XSS).
